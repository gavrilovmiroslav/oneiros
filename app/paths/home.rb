
get "/" do
	@name = "Ho!"
	haml :home
end

get "/:id" do
	id = params[:id] || "Mark"
	user = Queries.get("get_user").for(id)

	@name = user["name"] || "Helga"

	haml :home
end
