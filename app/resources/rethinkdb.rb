
require "rethinkdb"

db = RethinkDB::RQL.new

db.connect(
	:host => config.host,
	:port => config.port,
	:db   => config.db).repl
	
db