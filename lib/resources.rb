
require 'yaml' 
require 'hashie'

module Oneiros
	class Resources
		@resources = {}
		
		def initialize
		end
		
		def self.define(name)
			@resources[name] = OneirosResource.new(name)
		end
		
		def self.[](name)
			@resources[name].get
		end
		
		private
		
		class OneirosResource
			attr_accessor :ref, :config
			
			def initialize(name)
				config_file = "./app/config/#{name}.yml"
				@config = Hashie::Mash.new (YAML.load_file config_file)
			end
			
			def get
				@ref
			end
			
			def define				
				Proc.new {}
			end
		end
	end

	Dir["./app/resources/*.rb"].each do |resource|
		name = resource.split("/")[-1].gsub(".rb", "")
		
		res = Resources.define name
		res.ref = eval(File.read(resource), res.define.binding, resource)
	end
end