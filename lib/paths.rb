
module Oneiros
	module Paths
		class Base < Sinatra::Base
			configure do
      	set :root, File.join(File.expand_path("..", File.dirname(__FILE__)), "app")
      	set :views, Proc.new { File.join(root, "views") }
        set :public_folder, Proc.new { File.join(root, "public") }
			end
			
	  	Dir["./app/paths/*.rb"].each do |file|
  	  	res = Proc.new {}
  	  	eval(File.read(file), res.binding, file)
  	  	
  	  	res.call
  	  end
  	end
	end
end
