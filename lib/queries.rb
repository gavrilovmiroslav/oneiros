
module Oneiros
	module Queries
		@cache = {}
		
		def self.get(key)
			return @cache[key] if @cache.has_key? key
			file = "./app/queries/#{key}.rb"
			res = eval(File.read(file))			
			@cache[key] = res

			res
		end

		def self.resolve something
			return {} if something.nil?
			return something
		end		
	end
	
	class Query < Proc
		def for(*keys)
			self.call(*keys)
		end
	end
end
