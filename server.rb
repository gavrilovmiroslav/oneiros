
require 'rubygems'
require 'bundler'

require 'sinatra/base'
require 'haml'

$: << File.expand_path('../', __FILE__)

require 'lib/resources'
require 'lib/queries'

module Oneiros
	class Server < Sinatra::Base
		configure do
      set :root, Proc.new { File.join(File.dirname(__FILE__), "app") }
      set :views, Proc.new { File.join(root, "views") }
      
      set :public_folder, Proc.new { File.join(File.expand_path("..", root), "public") }

			set :logging, true
			set :dump_errors, true
			
			enable  :sessions
			set :session_secret, "everydoorisadreamofwhatliesbeyond"
			
			disable :method_override
			enable :static
		end
		
		use Rack::Deflater
		
		require 'lib/paths'

		use Oneiros::Paths::Base
	end
end